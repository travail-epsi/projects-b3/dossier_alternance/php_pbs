<!DOCTYPE html>
<html>

<head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <title>Restore CT or QEMU</title>
</head>

<body>

  <h3>Restore a Service</h3>
  <form method="POST" action="restoreService.php">
    <div class="row g-3">
      <div class="col-md-3">
        <select name="service" class="form-select" aria-label="Services">
          <option selected>Choose the service</option>
          <option value="100">Dolibarr</option>
          <option value="101">Nextcloud</option>
          <option value="102">Rocketchat</option>
          <option value="103">Wordpress</option>
          <option value="104">Bookstack</option>
          <option value="105">Moodle</option>
          <option value="106">NodeRED</option>
          <option value="107">Grafana</option>
          <option value="108">OnlyOffice</option>
          <option value="109">Matomo</option>
          <option value="110">pfSense</option>
          <option value="111">Nginx Reverse Proxy</option>
          <option value="112">Keycloak</option>
        </select>
      </div>
      <div class="col-md-3">
        <input type="text" class="form-control" placeholder="new vmid" name="vmid">
      </div>
      <div class="row g-3">
        <div class="col-md-3">
          <input type="text" class="form-control" placeholder="version" name="version">
        </div>
      </div>
      <div class="row g-3">
        <div class="col-12">
          <button type="submit" class="btn btn-primary">Setup</button>
        </div>
      </div>
  </form>

</body>

</html>