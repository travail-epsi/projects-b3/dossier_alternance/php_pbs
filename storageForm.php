<!DOCTYPE html>
<html>

<head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <title>Restore CT or QEMU</title>
</head>

<body>
  <h3>Setup Storage from PBS</h3>

  <form method="POST" action="createStorage.php">
    <div class="row g-3">
      <div class="col-md-2">
        <input type="text" class="form-control" placeholder="Server ip or fqdn" name="url">
      </div>
      <div class="col-md-2">
        <input type="text" class="form-control" placeholder="Datastore Name" name="datastore">
      </div>
      <div class="col-md-2">
        <input type="text" class="form-control" placeholder="Added Storage Name" name="name">
      </div>
    </div>

    <div class="row g-3">
      <div class="col-md-3">
        <input type="text" class="form-control" placeholder="Username" name="username">
      </div>
      <div class="col-md-3">
        <input type="password" class="form-control" placeholder="Password" name="password">
      </div>
    </div>

    <div class="row g-3">
      <div class="col-md-6">
        <input type="text" class="form-control" placeholder="Fingerprint" name="fingerprint">
      </div>
    </div>

    <div class="row g-3">
      <div class="col-md-6">
        <input type="text" class="form-control" placeholder="Encryption Key" name="encryption_key">
      </div>
    </div>

    <div class="row g-3">
      <div class="col-12">
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </div>
  </form>

</body>

</html>