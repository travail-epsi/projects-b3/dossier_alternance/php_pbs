<?php session_start();

require("ApiClass.php");

$api_json = json_decode($_SESSION["api"], true);
$api = new Api($api_json["ip"], $api_json["username"], $api_json["tokenId"], $api_json["token"], $api_json["node"], $api_json["storage"]);

$storage_json = json_decode($_SESSION["storage"], true);
$storage = new Storage($storage_json["name"], $storage_json["datastore"], $storage_json["encryption_key"], $storage_json["fingerprint"], $storage_json["url"], $storage_json["username"], $storage_json["password"]);

$version = (int)$_POST["version"] - 1;
$backups = $api->get_backups($storage, $_POST["service"]);
$backups = json_decode($backups);
$osTemplate = $backups->data[$version]->volid;
$format = $backups->data[$version]->format;

try {
  $res = null;
  switch ($format) {
    case "pbs-ct":
      $res = $api->restore_CT($osTemplate, $_POST["vmid"]);
      break;
    case "pbs-vm":
      $res = $api->restore_QM($_POST["service"]);
      break;
  }
  if ($res) {
    echo '<script>alert("Service Restored")</script>';
    echo '<script>window.location.replace("index.php");</script>';
  }
} catch (Exception $exep) {
  throw $exep;
}
