<?php
session_start();

require("ApiClass.php");

$api = null;
if (isset($_POST["token"])) {
  $api = new Api($_POST["ip"], $_POST["username"], $_POST["tokenId"], $_POST["token"], $_POST["node"], $_POST["storage"]);
  $api = json_encode($api);
  $_SESSION["api"] = $api;

  echo '<script>alert("Api Credientials Setup")</script>';
  echo '<script>window.location.replace("index.php");</script>';
} else {
  die();
}
