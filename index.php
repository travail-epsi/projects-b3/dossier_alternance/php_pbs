<?php session_start(); ?>
<!DOCTYPE html>
<html>

<head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
  <title>Restore CT or QEMU</title>
</head>
<?php
$iconApi = "";
if (isset($_SESSION["api"])) {
  $iconApi = "<i class='fa fa-check-square' style='color:#42f202'></i>";
}
$iconStorage = "";
if (isset($_SESSION["storage"])) {
  $iconStorage = "<i class='fa fa-check-square' style='color:#42f202'></i>";
}
?>

<body>
  <div class="table-responsive">
    <table class="table align-middle table-striped table-hover ">
      <thead>
        <th>#</th>
        <th>Action</th>
        <th>Form</th>
        <th></th>
      </thead>
      <tbody>
        <tr>
          <th>1</th>
          <td>Setup API</td>
          <td><a class="btn btn-primary" href="apiForm.php" role="button">apiForm</a></td>
          <td><?= $iconApi ?></td>
        </tr>
        <tr>
          <th>2</th>
          <td>Setup Storage</td>
          <td><a class="btn btn-primary" href="storageForm.php" role="button">storageForm</a></td>
          <td><?= $iconStorage ?></td>
        </tr>
        <tr>
          <th>3</th>
          <td>Restore</td>
          <td><a class="btn btn-primary" href="restoreForm.php" role="button">restoreForm</a></td>
          <td></td>
        </tr>
      </tbody>
    </table>
  </div>
</body>

</html>