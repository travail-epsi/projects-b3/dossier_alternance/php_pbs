<?php
require("StorageClass.php");
class Api implements JsonSerializable
{
  protected $ip;
  protected $username;
  protected $tokenId;
  protected $token;
  protected $node;
  protected $storage;

  /**
   * Api Constructor
   *
   * @param  string $ip
   * @param  string $username
   * @param  string $tokenId
   * @param  string $token
   * @param  string $node
   * @param  string $storage
   */
  public function __construct($ip, $username, $tokenId, $token, $node, $storage)
  {
    $this->set_ip($ip);
    $this->set_username($username);
    $this->set_tokenId($tokenId);
    $this->set_token($token);
    $this->set_node($node);
    $this->set_storage($storage);
  }

  ####ATRIBUTES#####
  #ip
  public function set_ip($ip)
  {
    $this->ip = $ip;
  }
  public function get_ip()
  {
    return $this->ip;
  }
  #storage
  public function set_storage($storage)
  {
    $this->storage = $storage;
  }
  public function get_storage()
  {
    return $this->storage;
  }
  #node
  public function set_node($node)
  {
    $this->node = $node;
  }
  public function get_node()
  {
    return $this->node;
  }

  #username
  public function set_username($username)
  {
    $this->username = $username;
  }
  public function get_username()
  {
    return $this->username;
  }

  #tokenId
  public function set_tokenId($tokenId)
  {
    $this->tokenId = $tokenId;
  }
  public function get_tokenId()
  {
    return $this->tokenId;
  }

  #token
  public function set_token($token)
  {
    $this->token = $token;
  }
  public function get_token()
  {
    return $this->token;
  }



  #####METHODES#####

  public function __toString()
  {
    return "base url: " . $this->get_base_url() . "</br> username: " . $this->get_username() . "</br> tokenId: " . $this->get_tokenId() . "</br> token: " . $this->get_token() . "</br>";
  }

  public function get_base_url()
  {
    return "https://" . $this->get_ip() . ":8006/api2/json";
  }

  public function get_authorization()
  {
    return "PVEAPIToken=" . $this->get_username() . "!" . $this->get_tokenId() . "=" . $this->get_token();
  }

  public function jsonSerialize(): mixed
  {
    return
      [
        'ip' => $this->get_ip(),
        'username' => $this->get_username(),
        'tokenId' => $this->get_tokenId(),
        'token' => $this->get_token(),
        'node' => $this->get_node(),
        'storage' => $this->get_storage()
      ];
  }

  ######API CALLS#####

  /**
   * create_storage
   *
   * Makes an api call to the distant Proxmox server to mount the pbs storage
   * 
   * @param  Storage $storage
   * @return string|bool|null
   */
  public function create_storage(Storage $storage)
  {
    $fields = "type=pbs&storage=" . rawurlencode($storage->get_name()) . "&datastore=" . rawurlencode($storage->get_datastore()) . "&encryption-key=" . rawurlencode($storage->get_encryption_key()) . "&fingerprint=" . rawurlencode($storage->get_fingerprint()) . "&password=" . rawurlencode($storage->get_password()) . "&server=" . rawurlencode($storage->get_url()) . "&username=" . rawurlencode($storage->get_username());

    //echo $fields;
    //echo "</br>";

    try {
      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => $this->get_base_url() . "/storage",
        //CURLOPT_VERBOSE => true,
        CURLOPT_SSL_VERIFYHOST => false,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>  $fields,
        CURLOPT_HTTPHEADER => array(
          "Authorization:" .  $this->get_authorization(),
          'Content-Type: application/x-www-form-urlencoded'
        ),
      ));

      $response = curl_exec($curl);
      if (curl_getinfo($curl, CURLINFO_HTTP_CODE) != 200 && curl_getinfo($curl, CURLINFO_HTTP_CODE) != 301) {
        echo $response;
        echo "</br>";
        echo "code: " . curl_getinfo($curl, CURLINFO_HTTP_CODE);
        echo "</br>";
        echo "url: " . curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
        echo "</br>";
        echo "curl error: " . curl_error($curl);
        $response = null;
      }

      curl_close($curl);
      return $response;
    } catch (Exception $e) {
      throw $e;
    }
  }


  /**
   * get_backups
   * 
   * Lists all the backups for a given QM or CT
   *
   * @param  Storage $storage
   * @param  int $vmid
   * @return string|bool|null
   */
  public function get_backups(Storage $storage, $vmid)
  {

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->get_base_url() . '/nodes/' . rawurlencode($this->get_node()) . '/storage/' . rawurlencode($storage->get_name()) . '/content/?vmid=' . $vmid,
      //CURLOPT_VERBOSE => true,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
        "Authorization:" .  $this->get_authorization()
      ),
    ));

    $response = curl_exec($curl);
    if (curl_getinfo($curl, CURLINFO_HTTP_CODE) != 200 && curl_getinfo($curl, CURLINFO_HTTP_CODE) != 301) {
      echo $response;
      echo "</br>";
      echo "code: " . curl_getinfo($curl, CURLINFO_HTTP_CODE);
      echo "</br>";
      echo "url: " . curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
      echo "</br>";
      echo "curl error: " . curl_error($curl);
    }
    curl_close($curl);
    return $response;
  }

  /**
   * restore_CT
   *
   * Restore a given CT with a new vmid
   * 
   * @param  string $osTemplate
   * @param  int $vmid
   * @return string|bool|null
   */
  public function restore_CT($osTemplate, $vmid)
  {
    $fields = "ostemplate=" . rawurlencode($osTemplate) . "&vmid=" . $vmid . "&restore=1&storage=" . rawurlencode($this->get_storage());

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->get_base_url() . "/nodes/" . rawurlencode($this->get_node()) . "/lxc",
      //CURLOPT_VERBOSE => true,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => $fields,
      CURLOPT_HTTPHEADER => array(
        "Authorization:" .  $this->get_authorization(),
        'Content-Type: application/x-www-form-urlencoded'
      ),
    ));

    $response = curl_exec($curl);
    if (curl_getinfo($curl, CURLINFO_HTTP_CODE) != 200 && curl_getinfo($curl, CURLINFO_HTTP_CODE) != 301) {
      echo $response;
      echo "</br>";
      echo "code: " . curl_getinfo($curl, CURLINFO_HTTP_CODE);
      echo "</br>";
      echo "url: " . curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
      echo "</br>";
      echo "curl error: " . curl_error($curl);
    }
    curl_close($curl);
    return $response;
  }

  /**
   * restore_QM
   *
   * Restore a given QM
   * 
   * @param  int $vmid
   * @return string|bool|null
   */
  public function restore_QM($vmid)
  {
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $this->get_base_url() . '/nodes/' . rawurlencode($this->get_node()) . '/qemu',
      //CURLOPT_VERBOSE => true,
      CURLOPT_SSL_VERIFYHOST => false,
      CURLOPT_SSL_VERIFYPEER => false,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS => 'vmid=' . $vmid . '&restore=1',
      CURLOPT_HTTPHEADER => array(
        "Authorization:" .  $this->get_authorization(),
        'Content-Type: application/x-www-form-urlencoded'
      ),
    ));

    $response = curl_exec($curl);
    if (curl_getinfo($curl, CURLINFO_HTTP_CODE) != 200 && curl_getinfo($curl, CURLINFO_HTTP_CODE) != 301) {
      echo $response;
      echo "</br>";
      echo "code: " . curl_getinfo($curl, CURLINFO_HTTP_CODE);
      echo "</br>";
      echo "url: " . curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
      echo "</br>";
      echo "curl error: " . curl_error($curl);
    }
    curl_close($curl);
    return $response;
  }
}
