<?php

class Storage implements JsonSerializable
{
  protected $name;
  protected $datastore;
  protected $encryption_key;
  protected $fingerprint;
  protected $url;
  protected $username;
  protected $password;

  /**
   * Storage Constuctor
   *
   * @param  string $name
   * @param  string $datastore
   * @param  string $encryption_key
   * @param  string $fingerprint
   * @param  string $url
   * @param  string $username
   * @param  string $password
   */
  function __construct($name, $datastore, $encryption_key, $fingerprint, $url, $username, $password)
  {
    $this->set_name($name);
    $this->set_datastore($datastore);
    $this->set_encryption_key($encryption_key);
    $this->set_fingerprint($fingerprint);
    $this->set_url($url);
    $this->set_username($username);
    $this->set_password($password);
  }

  ####ATRIBUTES#####

  #name
  function set_name($name)
  {
    $this->name = $name;
  }
  function get_name()
  {
    return $this->name;
  }

  #datastore
  function set_datastore($datastore)
  {
    $this->datastore = $datastore;
  }
  function get_datastore()
  {
    return $this->datastore;
  }

  #encryption_key
  function set_encryption_key($encryption_key)
  {
    $this->encryption_key = $encryption_key;
  }
  function get_encryption_key()
  {
    return $this->encryption_key;
  }

  #fingerprint
  function set_fingerprint($fingerprint)
  {
    $this->fingerprint = $fingerprint;
  }
  function get_fingerprint()
  {
    return $this->fingerprint;
  }

  #url
  function set_url($url)
  {
    $this->url = $url;
  }
  function get_url()
  {
    return $this->url;
  }

  #username
  function set_username($username)
  {
    $this->username = $username;
  }
  function get_username()
  {
    return $this->username;
  }

  #password
  function set_password($password)
  {
    $this->password = $password;
  }
  function get_password()
  {
    return $this->password;
  }

  #####METHODES#####

  public function jsonSerialize(): mixed
  {
    return
      [
        'name' => $this->get_name(),
        'datastore' => $this->get_datastore(),
        'encryption_key' => $this->get_encryption_key(),
        'fingerprint' => $this->get_fingerprint(),
        'url' => $this->get_url(),
        'username' => $this->get_username(),
        'password' => $this->get_password()
      ];
  }
}
