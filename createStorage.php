<?php
session_start();

require("ApiClass.php");
$api_json = json_decode($_SESSION["api"], true);
$api = new Api($api_json["ip"], $api_json["username"], $api_json["tokenId"], $api_json["token"], $api_json["node"], $api_json["storage"]);
$storage = null;
if (isset($api)) {
  $storage = new Storage($_POST["name"], $_POST["datastore"], $_POST["encryption_key"], $_POST["fingerprint"], $_POST["url"], $_POST["username"], $_POST["password"]);

  try {
    $res = $api->create_storage($storage);
    if ($res) {
      $storage = json_encode($storage);
      $_SESSION["storage"] = $storage;
      echo '<script>alert("Storage Create")</script>';
      echo '<script>window.location.replace("index.php");</script>';
    }
  } catch (Exception $exep) {
    throw $exep;
  }
} else {
  die();
}
